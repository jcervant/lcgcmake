cmake_minimum_required(VERSION 2.8.5)

# Declare the version of HEP Tools before including heptools-common
set(heptools_version devBE)
set(LCG_PYTHON_VERSION 3)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-common.cmake)

# We don't need ROOT in devBE but without a defined version CMake fails
LCG_AA_project(ROOT v6-18-00-patches)
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-base.cmake)

# Prepare the search paths according to the versions above
LCG_prepare_paths()
