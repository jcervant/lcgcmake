cmake_minimum_required(VERSION 2.8.5)

# Declare the version of HEP Tools we use
# (must be done before including heptools-common to allow evolution of the structure)
set(heptools_version  dev3cuda101)
set(LCG_PYTHON_VERSION 2)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-common.cmake)

# Please keep alphabetic order and the structure (tabbing).
# It makes it much easier to edit/read this file!

# Downgrade the C++ standard since Cuda (9) does not support c++17
#    This is a bit of hack since packages already build with a given hash value might have been built 
#    a different standard. It is OK for the nightlies for the time being.
if(LCG_CPP17)
  set(LCG_CPP17 FALSE)
  set(LCG_CPP14 TRUE)
endif()

LCG_AA_project(ROOT  HEAD)
LCG_external_package(hepmc3  githead)
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-base.cmake)


LCG_remove_package(tensorflow)
LCG_remove_package(tensorboard)
LCG_remove_package(tensorflow_estimator)


LCG_external_package(tensorflow        1.14.0                                 )
LCG_external_package(tensorflow_estimator  1.14.0                             )
LCG_external_package(tensorboard       1.14.0          protobuf=3.6.1         )

LCG_external_package(cuda              10.1                                  )
LCG_external_package(cudnn             7.6.1.34                              )

LCG_remove_package(rangev3)
LCG_external_package(rangev3  0.9.1 )

#---Additional External packages------(Generators)-----------------
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-generators.cmake)

# Prepare the search paths according to the versions above
LCG_prepare_paths()
