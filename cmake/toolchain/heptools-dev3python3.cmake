cmake_minimum_required(VERSION 2.8.5)

# Declare the version of HEP Tools we use
# (must be done before including heptools-common to allow evolution of the structure)
set(heptools_version  dev3python3)
set(LCG_PYTHON_VERSION 3)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-common.cmake)

# Please keep alphabetic order and the structure (tabbing).
# It makes it much easier to edit/read this file!

LCG_AA_project(ROOT  HEAD)
LCG_external_package(hepmc3  githead )
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-base.cmake)

#---Additional External packages------(Generators)-----------------
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-generators.cmake)

LCG_remove_package(Gaudi)
LCG_remove_package(rangev3)
LCG_external_package(rangev3  0.9.1 )

# Prepare the search paths according to the versions above
LCG_prepare_paths()
