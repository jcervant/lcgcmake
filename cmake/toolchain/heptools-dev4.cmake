cmake_minimum_required(VERSION 2.8.5)

# Declare the version of HEP Tools we use
# (must be done before including heptools-common to allow evolution of the structure)
set(heptools_version  dev4)
set(LCG_PYTHON_VERSION 2)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-common.cmake)

# Please keep alphabetic order and the structure (tabbing).
# It makes it much easier to edit/read this file!

LCG_AA_project(ROOT  v6-18-00-patches)
LCG_external_package(hepmc3  3.1.2)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-base.cmake)

#---Additional External packages------(Generators)-----------------
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-generators.cmake)

# Prepare the search paths according to the versions above
LCG_prepare_paths()
