cmake_minimum_required(VERSION 2.8.5)

# Declare the version of HEP Tools we use
# (must be done before including heptools-common to allow evolution of the structure)
set(heptools_version devgeantv)
set(LCG_PYTHON_VERSION 2)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-common.cmake)

# Please keep alphabetic order and the structure (tabbing).
# It makes it much easier to edit/read this file!

LCG_AA_project(ROOT 6.14.06)
LCG_external_package(hepmc3  3.0.0)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-base.cmake)

LCG_remove_package(veccore)
LCG_remove_package(vecmath)
LCG_remove_package(VecGeom)
LCG_remove_package(COOL)
LCG_remove_package(CORAL)

LCG_external_package(veccore           HEAD                                     )
LCG_external_package(vecmath           HEAD                                     )
LCG_external_package(VecGeom           HEAD                                     )

LCG_external_package(CASTOR          2.1.13-6               castor              )


#---Additional External packages------(Generators)-----------------
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-generators.cmake)

# Prepare the search paths according to the versions above
LCG_prepare_paths()
