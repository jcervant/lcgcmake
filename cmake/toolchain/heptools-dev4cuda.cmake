cmake_minimum_required(VERSION 2.8.5)

# Declare the version of HEP Tools we use
# (must be done before including heptools-common to allow evolution of the structure)
set(heptools_version  dev4cuda)
set(LCG_PYTHON_VERSION 3)

include(${CMAKE_CURRENT_LIST_DIR}/heptools-common.cmake)

# Please keep alphabetic order and the structure (tabbing).
# It makes it much easier to edit/read this file!

# Downgrade the C++ standard since Cuda (9) does not support c++17
#    This is a bit of hack since packages already build with a given hash value might have been built 
#    a different standard. It is OK for the nightlies for the time being.
if(LCG_CPP17)
  set(LCG_CPP17 FALSE)
  set(LCG_CPP14 TRUE)
endif()

LCG_AA_project(ROOT  v6-18-00-patches)
LCG_external_package(hepmc3  3.0.0 )

include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-base.cmake)

LCG_external_package(cuda              10.0                                 )
LCG_external_package(cudnn             7.6.1.34                             )

LCG_external_package(appdirs     1.4.3    )
LCG_external_package(py_tools    2019.1.1 )
LCG_external_package(pybind11    2.3.0    )
LCG_external_package(pyopencl    2019.1   )
LCG_external_package(pycuda      2019.1.1 )
LCG_external_package(mako        1.0.14   )
LCG_external_package(cupy        6.2.0    )
LCG_external_package(fastrlock   0.4      )

#---Additional External packages------(Generators)-----------------
include(${CMAKE_CURRENT_LIST_DIR}/heptools-dev-generators.cmake)

# Prepare the search paths according to the versions above
LCG_prepare_paths()
