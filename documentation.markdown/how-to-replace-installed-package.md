First, find the exact name of the installed package

    rpm --dbpath /afs/.cern.ch/sw/lcg/releases/var/lib/rpm -qa | grep <name>_<hash>

Then, remove the package:

    rpm --dbpath /afs/.cern.ch/sw/lcg/releases/var/lib/rpm --nodeps -evv <package>

Finally, rebuild a package using Jenkins.