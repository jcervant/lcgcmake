#---CASTOR-----------------------------------------------------------------------------------------------
LCGPackage_Add(
  CASTOR
  URL ${GenURL}/CASTOR-${CASTOR_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR>/usr <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  REVISION 1     # Force different installation avoding /usr
  BINARY_PACKAGE 1
)

#---cream------------------------------------------------------------------------------------------------
LCGPackage_Add(
  cream
  URL ${GenURL}/cream-${cream_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---dcap------------------------------------------------------------------------------------------------
LCGPackage_Add(
  dcap
  URL ${GenURL}/dcap-${dcap_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---dm-util-----------------------------------------------------------------------------------------------
LCGPackage_Add(
  dm-util
  URL ${GenURL}/dm-util-${dm-util_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---dpm---------------------------------------------------------------------------------------------------
LCGPackage_Add(
  dpm
  URL ${GenURL}/dpm-${dpm_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)


#---epel-------------------------------------------------------------------------------------------------
LCGPackage_Add(
  epel
  URL ${GenURL}/epel-${epel_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---FTS--------------------------------------------------------------------------------------------------
LCGPackage_Add(
  FTS
  URL ${GenURL}/FTS-${FTS_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---FTS3--------------------------------------------------------------------------------------------------
LCGPackage_Add(
  FTS3
  URL ${GenURL}/FTS3-${FTS3_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---gfal-------------------------------------------------------------------------------------------------
LCGPackage_Add(
  gfal
  URL ${GenURL}/gfal-${gfal_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---gfal2------------------------------------------------------------------------------------------------
LCGPackage_Add(
  gfal2
  URL ${GenURL}/gfal2-${gfal2_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---gridftp_ifce-----------------------------------------------------------------------------------------
LCGPackage_Add(
  gridftp_ifce
  URL ${GenURL}/gridftp-ifce-${gridftp_ifce_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---gridsite---------------------------------------------------------------------------------------------
LCGPackage_Add(
  gridsite
  URL ${GenURL}/gridsite-${gridsite_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---is_ifce---------------------------------------------------------------------------------------------
LCGPackage_Add(
  is_ifce
  URL ${GenURL}/is-ifce-${is_ifce_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---lb--------------------------------------------------------------------------------------------------
LCGPackage_Add(
  lb
  URL ${GenURL}/lb-${lb_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---WMS-------------------------------------------------------------------------------------------------
LCGPackage_Add(
  WMS
  URL ${GenURL}/WMS-${WMS_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---lcgdmcommon------------------------------------------------------------------------------------------
LCGPackage_Add(
  lcgdmcommon
  URL ${GenURL}/lcgdmcommon-${lcgdmcommon_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---lcginfosites-----------------------------------------------------------------------------------------
LCGPackage_Add(
  lcginfosites
  URL ${GenURL}/lcginfosites-${lcginfosites_native_version}-noarch.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---lfc-------------------------------------------------------------------------------------------------
LCGPackage_Add(
  lfc
  URL ${GenURL}/lfc-${lfc_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---srm_ifce---------------------------------------------------------------------------------------------
LCGPackage_Add(
  srm_ifce
  URL ${GenURL}/srm_ifce-${srm_ifce_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)

#---voms-------------------------------------------------------------------------------------------------
LCGPackage_Add(
  voms
  URL ${GenURL}/voms-${voms_native_version}-${LCG_ARCH}-${LCG_OS}${LCG_OSVERS}.tar.gz
  CONFIGURE_COMMAND <VOID>
  BUILD_COMMAND <VOID>
  INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory <SOURCE_DIR> <INSTALL_DIR>
  BUILD_IN_SOURCE 1
  BINARY_PACKAGE 1
)
